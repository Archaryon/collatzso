#include <stdio.h>
#include <sys/types.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char* argv[])
{

	char shm_name[] = "myshm";
	size_t shm_size = getpagesize();
	char *shm_ptr;
	int shm_fd;
shm_fd= shm_open(shm_name, O_CREAT|O_RDWR, S_IRUSR| S_IWUSR);
ftruncate(shm_fd,shm_size);
	
int i;
	int *ptr;
	int n;
	struct stat s;
	for(i=1;i<=argc;i++)
{
	int pid = fork();
	if(pid<0)
	{
	printf("Error\n");
	return 0;}
	else 
	if(pid==0)
{

shm_ptr=mmap(0,shm_size, PROT_WRITE, MAP_SHARED, shm_fd, 0);
if(shm_ptr == MAP_FAILED){
perror(NULL);
shm_unlink(shm_name);
return errno;

}

if(shm_fd < 0) 
{
	perror(NULL);
	return errno;
}
	

	printf("%d ",n);
	n=atoi(argv[i]);
	while(n!=1)
	{
		if(n%2==0)
			n=n/2;
	else n=n*3+1;
	
	printf("%d ",n);
	int sizec=sprintf(shm_ptr,"%d",n);
	shm_ptr+=sizec;

}
	printf("\n");
	printf("Done Parent %d Me %d\n",getppid(),getpid());
	exit(1);
}

	else if(pid>0)
	{
	printf("Starting Parent %d \n", getpid());


	}
}	
shm_unlink(shm_name);
munmap(shm_ptr,shm_size);	
return 0;

}
